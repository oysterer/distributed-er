package sortingclosure;

public class Pair implements Comparable<Pair> {
String key, value;
	
	public Pair(){
	}
	
	public Pair(String s1, String s2){
		key = s1;
		value = s2;
	}
	
	public void setKey(String in){
		key = in;
	}
	public void setValue(String in){
		value = in;
	}
	public String getKey(){
		return key;
	}
	public String getValue(){
		return value;
	}
	
	public int compareTo(Pair cp){
		int result = key.compareTo(cp.getKey());
		if (result==0) {
			return value.compareTo(cp.getValue());
		} else {
			return result;
		}
	}
	
	public boolean isEqualTo(Pair pr){
		if ((this.key).equals(pr.getKey()) && (this.value).equals(pr.getValue())) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean hasSameKeyAs(Pair pr){
		if ((this.key).equals(pr.getKey())) {
			return true;
		} else {
			return false;
		}	
	}
	
	public Pair reversePair(){
		Pair result = new Pair();
		result.setKey(value);
		result.setValue(key);
		return result;
	}
	
	public Boolean keySameAsValue(){
		if(key.equals(value)) return true; else return false;
	}
	
}
